package com.lpsoft.web.dto;

public class ResultDto {

    private boolean status;
    private String result;
    private String messageError;

    public ResultDto(boolean status, String result, String messageError) {
        this.status = status;
        this.result = result;
        this.messageError = messageError;
    }

    public ResultDto() {
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getMessageError() {
        return messageError;
    }

    public void setMessageError(String messageError) {
        this.messageError = messageError;
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
