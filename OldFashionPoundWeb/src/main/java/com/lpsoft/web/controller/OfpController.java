package com.lpsoft.web.controller;

import com.lpsoft.euris.OldFashionPound;
import com.lpsoft.euris.OldFashionPoundOperator;
import com.lpsoft.euris.exception.OldFashionPoundException;
import com.lpsoft.web.dto.ResultDto;
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class OfpController {


    @GetMapping(path = "/getSum", produces = "application/json")
    public ResponseEntity<ResultDto> getSum(String a, String b){
        ResultDto result = new ResultDto();
        OldFashionPound oldFashionPound = new OldFashionPound(a,b);
        oldFashionPound.setOperator(OldFashionPoundOperator.SUM);
        try {
            String r = oldFashionPound.executeOperation();
            result.setStatus(true);
            result.setResult(r);

        }catch (OldFashionPoundException ofpe){
            result.setStatus(false);
            result.setMessageError(ofpe.getMessage());

        }catch (Exception e){
            result.setStatus(false);
            result.setMessageError("Generic Error");
        }
        HttpHeaders responseHeaders = new HttpHeaders();
        return new ResponseEntity<ResultDto>(result, responseHeaders, HttpStatus.CREATED);
    }

    @GetMapping(path = "/getSubstruction", produces = "application/json")
    public ResponseEntity<ResultDto> getSubstruction(String a, String b){
        ResultDto result = new ResultDto();
        OldFashionPound oldFashionPound = new OldFashionPound(a,b);
        oldFashionPound.setOperator(OldFashionPoundOperator.SUBSTRUCTION);
        try {
            String r = oldFashionPound.executeOperation();
            result.setStatus(true);
            result.setResult(r);

        }catch (OldFashionPoundException ofpe){
            result.setStatus(false);
            result.setMessageError(ofpe.getMessage());

        }catch (Exception e){
            result.setStatus(false);
            result.setMessageError("Generic Error");
        }
        HttpHeaders responseHeaders = new HttpHeaders();
        return new ResponseEntity<ResultDto>(result, responseHeaders, HttpStatus.CREATED);
    }

    @GetMapping(path = "/getMultiplication", produces = "application/json")
    public ResponseEntity<ResultDto> getMultiplication(String a, String b){
        ResultDto result = new ResultDto();
        OldFashionPound oldFashionPound = new OldFashionPound(a,b);
        oldFashionPound.setOperator(OldFashionPoundOperator.MULTIPLICATION);
        try {
            String r = oldFashionPound.executeOperation();
            result.setStatus(true);
            result.setResult(r);

        }catch (OldFashionPoundException ofpe){
            result.setStatus(false);
            result.setMessageError(ofpe.getMessage());

        }catch (Exception e){
            result.setStatus(false);
            result.setMessageError("Generic Error");
        }
        HttpHeaders responseHeaders = new HttpHeaders();
        return new ResponseEntity<ResultDto>(result, responseHeaders, HttpStatus.CREATED);
    }

    @GetMapping(path = "/getDivision", produces = "application/json")
    public ResponseEntity<ResultDto> getDivision(String a, String b){
        ResultDto result = new ResultDto();
        OldFashionPound oldFashionPound = new OldFashionPound(a,b);
        oldFashionPound.setOperator(OldFashionPoundOperator.DIVISION);
        try {
            String r = oldFashionPound.executeOperation();
            result.setStatus(true);
            result.setResult(r);

        }catch (OldFashionPoundException ofpe){
            result.setStatus(false);
            result.setMessageError(ofpe.getMessage());

        }catch (Exception e){
            result.setStatus(false);
            result.setMessageError("Generic Error");
        }
        HttpHeaders responseHeaders = new HttpHeaders();
        return new ResponseEntity<ResultDto>(result, responseHeaders, HttpStatus.CREATED);
    }
}
