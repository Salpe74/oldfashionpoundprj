# README #

OldFashionPound: 
libreria che implementa le 4 operazioni fondamentali secondo il sistema monetario anglosassone antecedente al 1970 (1 sterlina=20 scellini, 1 scellino=12 pennies).
L'algorimtmo utilizzato è quello di riparametrizzare i prezzi al taglio più piccolo, i pennies, eseguire le operazioni tra interi e poi ritrasformare il risiìultato
nella notazione corretta.

### struttura del progetto ###

OldFashionPound : libreria 
OldFashionPoundWeb : backend API rest per il test
OldFashionPoundFE : Frontend Angular per il test


E' sviluppato utilizzando Java SDK 12 senza l'ausilio di librerie di terze parti.
In out ci sono già le classi buildate e un artifact di tipo .jar.


E' possibile testarlo sia attraverso la classe main di test OldFashionPoundTest oppure attraverso i progetti OldFashionPoundWeb e OldFashionPoundFE 
che implementano una piccola web application di test. 

OldFashionPoundWeb è una spring application che espone le API per le operazioni della libreria OldFashionPound, che importa come jar
in /lib.
E' stata sviluppata con Intellij e testata con il tomcat interno di Intellij stesso.


OldFashionPoundFE è un FE sviluppato in Angular. Non utilizza particolari dipendenze quindi basta un npm install e un ng serve per farlo 
compilare e startarlo. Presuppone che le API siano raggiungibili in localhost:8080

Per un utilizzo immediato ho versionato anche i compilati...
