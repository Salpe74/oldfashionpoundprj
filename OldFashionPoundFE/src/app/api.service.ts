import { Injectable } from '@angular/core';
import { HttpClient} from "@angular/common/http";
import { HttpHeaders } from '@angular/common/http';
import { HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

 headers = new HttpHeaders({
    'Access-Control-Allow-Origin':'http://localhost:8080',
    'Access-Control-Allow-Headers': 'x-requested-with, Content-Type, origin, authorization, accept, client-security-token',
    'Access-Control-Allow-Methods': 'GET, POST, OPTIONS, PUT, PATCH, DELETE'
  });

  constructor(private httpClient: HttpClient) { }

  public getSum(value1: string, value2: string){
    let params = new HttpParams();
    params = params.append('a', value1);
    params = params.append('b', value2);
    let httpOptions = {
      headers: this.headers,
      params : params
    };

    return this.httpClient.get('http://localhost:8080/getSum?',  httpOptions );
  }

  public getSubstr(value1: string, value2: string){
    let params = new HttpParams();
    params = params.append('a', value1);
    params = params.append('b', value2);
    let httpOptions = {
      headers: this.headers,
      params : params
    };

    return this.httpClient.get('http://localhost:8080/getSubstruction?',  httpOptions );
  }

  public getMultiplication(value1: string, value2: string){
    let params = new HttpParams();
    params = params.append('a', value1);
    params = params.append('b', value2);
    let httpOptions = {
      headers: this.headers,
      params : params
    };

    return this.httpClient.get('http://localhost:8080/getMultiplication?',  httpOptions );
  }

  public getDivision(value1: string, value2: string){
    let params = new HttpParams();
    params = params.append('a', value1);
    params = params.append('b', value2);
    let httpOptions = {
      headers: this.headers,
      params : params
    };

    return this.httpClient.get('http://localhost:8080/getDivision?',  httpOptions );
  }
}
