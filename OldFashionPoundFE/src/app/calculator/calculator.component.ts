import { Component } from '@angular/core';
import {ApiService} from "../api.service";

@Component({
  selector: 'app-calculator',
  templateUrl: './calculator.component.html',
  styleUrls: ['./calculator.component.css']
})
export class CalculatorComponent {
  result;
  constructor(private apiService: ApiService) {
    // this.apiService.getPippo().subscribe((p)=>{
    //
    //   console.log(p);
    //   this.pippo=p['operationTerm1'];
    // });
  }

  getSum(value1: string, value2: string){

    this.apiService.getSum(value1,value2).subscribe((p)=>{
      let status = p['status'];
      if(status==true) {
        this.result=p['result'];
      }
      else{
        this.result=p['messageError'];
      }
    });

  }
  getSubstr(value1: string, value2: string){

    this.apiService.getSubstr(value1,value2).subscribe((p)=>{
      let status = p['status'];
      if(status==true) {
        this.result=p['result'];
      }
      else{
        this.result=p['messageError'];
      }
    });

  }

  getMultiplication(value1: string, value2: string){

    this.apiService.getMultiplication(value1,value2).subscribe((p)=>{
      let status = p['status'];
      if(status==true) {
        this.result=p['result'];
      }
      else{
        this.result=p['messageError'];
      }
    });

  }

  getDivision(value1: string, value2: string){

    this.apiService.getDivision(value1,value2).subscribe((p)=>{
      let status = p['status'];
      if(status==true) {
        this.result=p['result'];
      }
      else{
        this.result=p['messageError'];
      }
    });

  }


}


/*
Copyright Google LLC. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/
