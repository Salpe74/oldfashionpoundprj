package com.lpsoft.euris.exception;

public class OldFashionPoundException extends  RuntimeException{

    public OldFashionPoundException(String message, Throwable cause) {
        super(message, cause);
    }
    public OldFashionPoundException(String message) {
        super(message);
    }
}
