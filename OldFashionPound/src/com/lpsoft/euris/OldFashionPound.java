package com.lpsoft.euris;


import com.lpsoft.euris.bean.OFPPrice;
import com.lpsoft.euris.exception.OldFashionPoundException;
import com.lpsoft.euris.helper.Calculator;
import com.lpsoft.euris.helper.Tools;


public class OldFashionPound {

    private String  operationTerm1;
    private String  operationTerm2;
    private OldFashionPoundOperator operator;

    public OldFashionPound() {
    }

    public OldFashionPound(String _operationTerm1, String _operationTerm2, OldFashionPoundOperator _operator) {
        this.operationTerm1 = _operationTerm1;
        this.operationTerm2 = _operationTerm2;
        this.operator = _operator;
    }

    public OldFashionPound(String _operationTerm1, String _operationTerm2) {
        this.operationTerm1 = _operationTerm1;
        this.operationTerm2 = _operationTerm2;
    }

    public String getOperationTerm1() {
        return operationTerm1;
    }

    public void setOperationTerm1(String _operationTerm1) {
        this.operationTerm1 = _operationTerm1;
    }

    public String getOperationTerm2() {
        return operationTerm2;
    }

    public void setOperationTerm2(String _operationTerm2) {
        this.operationTerm2 = _operationTerm2;;
    }

    public String getOperator() {
        return operator.toString();
    }

    public String getOperatorString() {
        return operator.operatorString;
    }

    public void setOperator(OldFashionPoundOperator _operator) {
        this.operator = _operator;
    }

    public String executeOperation(){
        OFPPrice resultObj = null;
        String result = "";
        try{
            if( operator!=null && !operator.equals("")) {

                switch (operator) {
                    case SUM:
                        resultObj  = Calculator.sum(Tools.convertStringToObj(operationTerm1),Tools.convertStringToObj(operationTerm2));
                        break;
                    case SUBSTRUCTION:
                        resultObj  = Calculator.subtraction(Tools.convertStringToObj(operationTerm1),Tools.convertStringToObj(operationTerm2));
                        break;
                    case DIVISION:
                        resultObj  = Calculator.division(Tools.convertStringToObj(operationTerm1),Tools.validateIntOperator(operationTerm2));
                        break;
                    case MULTIPLICATION:
                        resultObj  = Calculator.multiplication(Tools.convertStringToObj(operationTerm1),Tools.validateIntOperator(operationTerm2));
                        break;
                    default:
                        throw new IllegalArgumentException("The Operator " + operator + " is not a valid argument. Accepted Argument (+,-,/,*) ");
                }
            }
            else{
                throw new IllegalArgumentException("Operator Null or empty.");
            }
            return resultObj.toString();

        }
        catch (OldFashionPoundException ofpe){
            throw ofpe;
        }catch (IllegalArgumentException ie){
            throw ie;
        }catch (Exception e){
            throw new OldFashionPoundException("Generic Exception", e);
        }
    }
}
