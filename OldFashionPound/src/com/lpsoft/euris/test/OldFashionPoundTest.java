package com.lpsoft.euris.test;


import com.lpsoft.euris.OldFashionPound;
import com.lpsoft.euris.OldFashionPoundOperator;

public class OldFashionPoundTest {

    public static void main(String[] args) {

        OldFashionPound oldFashionPound = new OldFashionPound("5p 17s","3p 4s 10d");
        oldFashionPound.setOperator(OldFashionPoundOperator.SUM);
        System.out.println(oldFashionPound.getOperator());
        System.out.println(oldFashionPound.getOperationTerm1() + " " + oldFashionPound.getOperatorString() + " " + oldFashionPound.getOperationTerm2() + " = " + oldFashionPound.executeOperation());

        oldFashionPound.setOperator(OldFashionPoundOperator.SUBSTRUCTION);
        System.out.println(oldFashionPound.getOperator());
        System.out.println(oldFashionPound.getOperationTerm1() + " " + oldFashionPound.getOperatorString() + " " + oldFashionPound.getOperationTerm2() + " = " + oldFashionPound.executeOperation());

        oldFashionPound.setOperator(OldFashionPoundOperator.SUBSTRUCTION);
        oldFashionPound.setOperationTerm1("3p 4s 10d");
        oldFashionPound.setOperationTerm2("5p 17s 8d");
        System.out.println(oldFashionPound.getOperationTerm1() + " " + oldFashionPound.getOperatorString() + " " + oldFashionPound.getOperationTerm2() + " = " + oldFashionPound.executeOperation());

        oldFashionPound.setOperator(OldFashionPoundOperator.MULTIPLICATION);
        oldFashionPound.setOperationTerm1("5p 17s 8d");
        oldFashionPound.setOperationTerm2("2");
        System.out.println(oldFashionPound.getOperator());
        System.out.println(oldFashionPound.getOperationTerm1()+ " " + oldFashionPound.getOperatorString() + " " + oldFashionPound.getOperationTerm2() + " = " + oldFashionPound.executeOperation());

        oldFashionPound.setOperator(OldFashionPoundOperator.DIVISION);
        oldFashionPound.setOperationTerm2("3");
        System.out.println(oldFashionPound.getOperator());
        System.out.println(oldFashionPound.getOperationTerm1() + " " + oldFashionPound.getOperatorString() + " " + oldFashionPound.getOperationTerm2() + " = " + oldFashionPound.executeOperation());

        oldFashionPound.setOperator(OldFashionPoundOperator.DIVISION);
        oldFashionPound.setOperationTerm1("18p 16s 1d");
        oldFashionPound.setOperationTerm2("15");
        System.out.println(oldFashionPound.getOperationTerm1() + " " + oldFashionPound.getOperatorString() + " " + oldFashionPound.getOperationTerm2() + " = " + oldFashionPound.executeOperation());


    }
}
