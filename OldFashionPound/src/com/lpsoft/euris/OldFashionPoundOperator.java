package com.lpsoft.euris;

public enum OldFashionPoundOperator {

    SUM("+"),
    SUBSTRUCTION("-"),
    MULTIPLICATION("*"),
    DIVISION("/");

    public final String operatorString;

    OldFashionPoundOperator(String operatorString) {
        this.operatorString = operatorString;
    }
}
