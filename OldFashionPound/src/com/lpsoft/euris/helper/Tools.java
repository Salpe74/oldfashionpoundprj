package com.lpsoft.euris.helper;

import com.lpsoft.euris.bean.OFPPrice;
import com.lpsoft.euris.exception.OldFashionPoundException;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Tools {

    public static OFPPrice convertStringToObj (String price)  {
        OFPPrice objRet = null;
        try{
            String regex1 = "(.?\\d)p (.?\\d)s (.?\\d)d";
            String regex2 = "(.?\\d)p (.?\\d)s";
            String regex3 = "(.?\\d)p (.?\\d)d";
            String regex4 = "(.?\\d)s (.?\\d)d";
            String regex5 = "(.?\\d)p";
            String regex6 = "(.?\\d)s";
            String regex7 = "(.?\\d)d";
            if(price == null) return objRet;
            Pattern pattern = Pattern.compile(regex1);
            Matcher match = pattern.matcher(price);
            if(match.find()) {
                int p = Integer.parseInt(match.group(1));
                int s = Integer.parseInt(match.group(2));
                int d = Integer.parseInt(match.group(3));
                objRet =  new OFPPrice(p,s,d);
            }else {
                pattern = Pattern.compile(regex2);
                match = pattern.matcher(price);
                if (match.find()) {
                    int p = Integer.parseInt(match.group(1));
                    int s = Integer.parseInt(match.group(2));
                    objRet = new OFPPrice(p, s, 0);
                } else {
                    pattern = Pattern.compile(regex3);
                    match = pattern.matcher(price);
                    if (match.find()) {
                        int p = Integer.parseInt(match.group(1));
                        int d = Integer.parseInt(match.group(2));
                        objRet = new OFPPrice(p, 0, d);
                    } else {
                        pattern = Pattern.compile(regex4);
                        match = pattern.matcher(price);
                        if (match.find()) {
                            int s = Integer.parseInt(match.group(1));
                            int d = Integer.parseInt(match.group(2));
                            objRet = new OFPPrice(0, s, d);
                        } else {
                            pattern = Pattern.compile(regex5);
                            match = pattern.matcher(price);
                            if (match.find()) {
                                int p = Integer.parseInt(match.group(1));
                                objRet = new OFPPrice(p, 0,0);
                            } else {
                                pattern = Pattern.compile(regex6);
                                match = pattern.matcher(price);
                                if (match.find()) {
                                    int s = Integer.parseInt(match.group(1));
                                    objRet = new OFPPrice(0, s,0);
                                } else {
                                    pattern = Pattern.compile(regex7);
                                    match = pattern.matcher(price);
                                    if (match.find()) {
                                        int d = Integer.parseInt(match.group(1));
                                        objRet = new OFPPrice(0, 0,d);
                                    } else {
                                        throw new OldFashionPoundException("The given price (" + price + ") is not in a valid format. Accepted formats : (Xp Xs Xd) or (Xp Xs) or (Xp Xd) or " +
                                                "(Xs Xd) or (Xp) or (Xs) or (Xd)");
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return objRet;

        }catch(OldFashionPoundException ofpe){
            throw ofpe;
        }catch(Exception e){
            throw new OldFashionPoundException("Conversion error", e);
        }
    }

    public static int convertObjToInt (OFPPrice obj)  {
        try{
            int p = obj.getPound();
            int s = obj.getShilling();
            int d = obj.getPence();
            int ret = d + (s*12) + (p*240);
            return ret;

        }catch(Exception e){
            throw new OldFashionPoundException("Conversion error", e);
        }
    }

    public static OFPPrice convertIntToObj (int i)  {
        OFPPrice objRet = null;
        try{
            int p = i/240;
            int s = (i%240)/12;
            int d = ((i%240)%12);
            objRet =  new OFPPrice(p,s,d);
            return objRet;

        }catch(Exception e){
            throw new OldFashionPoundException("Conversion error", e);
        }


    }

    public static int validateIntOperator(String op){
        try {
          return Integer.parseInt(op);

        }catch (NumberFormatException nfe){
            throw new OldFashionPoundException("Illegal operationTerm : " + op + ". Divider and Multiplier term must be integer.", nfe);
        }
    }
}
