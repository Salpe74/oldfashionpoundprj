package com.lpsoft.euris.helper;

import com.lpsoft.euris.bean.OFPPrice;
import com.lpsoft.euris.exception.OldFashionPoundException;

public class Calculator {

    public static OFPPrice sum (OFPPrice a , OFPPrice b ){
        OFPPrice objRet = null;
        try{
            int addend1 = Tools.convertObjToInt(a);
            int addend2 = Tools.convertObjToInt(b);
            int result = addend1 + addend2;
            objRet = Tools.convertIntToObj(result);
            return objRet;

        }catch(OldFashionPoundException ofpe){
            throw ofpe;
        }catch(Exception e){
            throw new OldFashionPoundException("Operation Exception.", e);
        }

    }

    public static OFPPrice subtraction (OFPPrice a , OFPPrice b ){
        OFPPrice objRet = null;
        try{
            int addend1 = Tools.convertObjToInt(a);
            int addend2 = Tools.convertObjToInt(b);
            if(addend1 > addend2){
                int result = addend1 - addend2;
                objRet = Tools.convertIntToObj(result);
            }
            else{
                int result = addend2 - addend1;
                objRet = Tools.convertIntToObj(result);
                objRet.setSign("-");
            }
            return objRet;

        }catch(OldFashionPoundException ofpe){
            throw ofpe;
        }catch(Exception e){
            throw new OldFashionPoundException("Operation Exception.", e);
        }

    }

    public static OFPPrice multiplication (OFPPrice a , int multiplier ){
        OFPPrice objRet = null;
        try{
            int factor1 = Tools.convertObjToInt(a);
            int result = factor1 * multiplier;
            objRet = Tools.convertIntToObj(result);
            return objRet;
        }catch(OldFashionPoundException ofpe){
            throw ofpe;
        }catch(Exception e){
            throw new OldFashionPoundException("Operation Exception.", e);
        }

    }

    public static OFPPrice division (OFPPrice a , int divider ){
        OFPPrice objRet = null;
        try{
            int factor1 = Tools.convertObjToInt(a);
            int result = factor1 / divider;
            int divisionReminder = factor1 % divider;
            objRet = Tools.convertIntToObj(result);
            objRet.setDivisionReminder(divisionReminder);
            return objRet;
        }catch(OldFashionPoundException ofpe){
            throw ofpe;
        }catch(Exception e){
            throw new OldFashionPoundException("Operation Exception.", e);
        }

    }

}
