package com.lpsoft.euris.bean;

import com.lpsoft.euris.helper.Tools;

public class OFPPrice implements java.io.Serializable{

    private int pound;
    private int shilling;
    private int pence;

    private int divisionReminder = 0;
    private String  sign = "+";

    public OFPPrice(int pound, int shilling, int pence) {
        this.pound = pound;
        this.shilling = shilling;
        this.pence = pence;
    }

    public OFPPrice(String price) {
    }

    public OFPPrice(String pound, String shilling, String pence) {
    }

    public int getPound() {
        return pound;
    }

    public void setPound(int pound) {
        this.pound = pound;
    }

    public int getShilling() {
        return shilling;
    }

    public void setShilling(int shilling) {
        this.shilling = shilling;
    }

    public int getPence() {
        return pence;
    }

    public void setPence(int pence) {
        this.pence = pence;
    }

    public int getDivisionReminder() {
        return divisionReminder;
    }

    public void setDivisionReminder(int divisionReminder) {
        this.divisionReminder = divisionReminder;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    @Override
    public String toString() {
        String ret = (pound>0 || shilling>0 || pence>0) ? ((pound>0) ? pound + "p " : "" )+ ((shilling>0) ? shilling + "s " : ((shilling == 0 && pound>0) ? "0s " : ""))  + ((pence>0)? pence + "d" : ((pence == 0 && (pound>0 ||shilling>0) ) ? "0d" : "")) : "0";
        if (!sign.equals("+")) ret = sign + ret;
        if (divisionReminder>0){
            OFPPrice ofpPrice = Tools.convertIntToObj(divisionReminder);
            ret = ret + " (" + ofpPrice.toString() + ")";
        }
        return ret;
    }
}
